# OpenML dataset: collins

https://www.openml.org/d/40971

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Jeff Collins  
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/collins.txt)  
**Please cite**: None  

Data used in an analysis of the Brown and Frown corpora for my doctoral dissertation titled ``Variations in Written English: Characterizing Authors' Rhetorical Language Choices Across Corpora of Published Texts" (Completed at Carnegie Mellon Univ, 2003).  The source of the corpora was the ICAME CD-ROM  (get info at <http>).

The data were generated from the texts using tagging and visualization software, Docuscope.

The first row is the variable names. The genre of each text (assigned by the Brown corpus compilers) is in 'Genre' column and the corpus is listed in the 'corpus' column with 1=Brown and 2=Frown corpus.

The dataset may be freely used and distributed for non-commercial purposes.

Note: The Genre and Corpus values together make up the target, and the Countr just counts documents within each counter, so they should probably be ignored.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40971) of an [OpenML dataset](https://www.openml.org/d/40971). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40971/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40971/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40971/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

